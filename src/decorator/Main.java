/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        PizzaIF pizza = new Ananas(new Kinkku(new Tomaattikastike(new Pohja())));
        
        System.out.println("Pizzan kokonaishinta: " + pizza.getHinta());
        System.out.println("Pizzan sisältö: " + pizza.getKuvaus());
        
    }
    
}
