/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

/**
 *
 * @author Toni
 */
public class Tomaattikastike extends PizzaDecorator{
    
    int hinta = 2;
    String kuvaus = ", tomaattikastike";

    public Tomaattikastike(PizzaIF pizza) {
        super(pizza);
    }
    
    @Override
    public int getHinta() {
        return super.getHinta() + hinta;
    }
    
    @Override
    public String getKuvaus() {
        return super.getKuvaus() + kuvaus;
    }
    
    
}
