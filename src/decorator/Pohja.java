/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

/**
 *
 * @author Toni
 */
public class Pohja implements PizzaIF {
    
    int hinta = 3;
    String kuvaus = "Pizzan pohja";

    public int getHinta() {
        return hinta;
    }

    public String getKuvaus() {
        return kuvaus;
    }
    
}
