/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

/**
 *
 * @author Toni
 */
public abstract class PizzaDecorator implements PizzaIF {

    protected PizzaIF pizza;

    public PizzaDecorator(PizzaIF pizza) {
        this.pizza = pizza;
    }
    
    public int getHinta() {
        return pizza.getHinta();
    }

    public String getKuvaus() {
        return pizza.getKuvaus();
    }

}
