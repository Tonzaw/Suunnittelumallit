/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Toni
 */
public class MyThreadD extends Thread {

    Iterator<Integer> i;
    String nimi;

    public MyThreadD(Iterator<Integer> i, String nimi) {
        this.nimi = nimi;
        this.i = i;
    }

    public void run() {
        while (this.i.hasNext()) {
            Integer num = this.i.next();
            System.out.println(nimi + " : " + num);
            if (num > 100){
                try {
                    this.join();
                } catch (InterruptedException ex) {
                    Logger.getLogger(MyThreadD.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
