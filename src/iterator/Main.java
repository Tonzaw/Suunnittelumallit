/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        ArrayList<Integer> list = new ArrayList<Integer>();

        for (int i = 0; i < 100; i++) {
            list.add(i*2);
        }
        
        Iterator<Integer> i = list.iterator();
        //A Tehtävä:
        /*
        Iterator<Integer> i2 = list.iterator();

        MyThread threadOne = new MyThread(i, "Säie yksi");
        MyThread threadTwo = new MyThread(i2, "Säie kaksi");

        threadOne.start();
        threadTwo.start();
        */
        
        //B Tehtävä:
        /*
        MyThreadB threadOne = new MyThreadB(i, "Säie yksi");
        MyThreadB threadTwo = new MyThreadB(i, "Säie kaksi");

        threadOne.start();
        threadTwo.start();
        */
        
        //C Tehtävä:
        /*
        MyThreadC threadOne = new MyThreadC(i, "Säie yksi");
        MyThreadC threadTwo = new MyThreadC(i, "Säie kaksi");

        threadOne.start();
        threadTwo.start();
        */
        
        //D Tehtävä:
        
        MyThreadD threadOne = new MyThreadD(i, "Säie yksi");
        MyThreadD threadTwo = new MyThreadD(i, "Säie kaksi");

        threadOne.start();
        threadTwo.start();
        
    }

}
