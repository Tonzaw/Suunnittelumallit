/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iterator;

import java.util.Iterator;

/**
 *
 * @author Toni
 */
public class MyThreadC extends Thread {

    Iterator<Integer> i;
    String nimi;

    public MyThreadC(Iterator<Integer> i, String nimi) {
        this.nimi = nimi;
        this.i = i;
    }

    public void run() {
        while (this.i.hasNext()) {
            Integer num = this.i.next();
            System.out.println(nimi + " : " + num);
            i.remove();
        }
    }
}
