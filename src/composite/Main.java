package composite;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Laiteosa kokonaisuus = new KoteloComplex();
        
        Laiteosa näytönohjain = new Näytönohjain();
        kokonaisuus.lisääLaiteosa(näytönohjain);
        
        Laiteosa emolevy = new EmolevyComplex();
        Laiteosa prosessori = new Prosessori();
        Laiteosa muistipiiri = new Muistipiiri();
        emolevy.lisääLaiteosa(prosessori);
        emolevy.lisääLaiteosa(muistipiiri);
        kokonaisuus.lisääLaiteosa(emolevy);
        
        Laiteosa verkkokortti = new Verkkokortti();
        kokonaisuus.lisääLaiteosa(verkkokortti);
        
        System.out.println("Kokonaishinta: " + kokonaisuus.haeHinta(kokonaisuus) + "€");
        
    }
    
}
