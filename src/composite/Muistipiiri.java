/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

/**
 *
 * @author Toni
 */
public class Muistipiiri implements Laiteosa {
    int hinta = 50;

    public int haeHinta(Laiteosa laiteosa) {
        return hinta;
    }

    public void lisääLaiteosa(Laiteosa laiteosa) {
        throw new UnsupportedOperationException("Tähän laiteosaan ei voida lisätä toista osaa.");
    }
}
