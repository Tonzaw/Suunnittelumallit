/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Toni
 */
public class KoteloComplex implements Laiteosa {
    
    List<Laiteosa> laiteosalista = new ArrayList<Laiteosa>();
    
    int hinta = 80;
    
    public int haeHinta(Laiteosa laiteosa) {
        for (Laiteosa l : laiteosalista){
            hinta = hinta + l.haeHinta(laiteosa);
        }
        return hinta;
    }

    public void lisääLaiteosa(Laiteosa laiteosa) {
        laiteosalista.add(laiteosa);
    }
}