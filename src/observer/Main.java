/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Digitaalinenkello kello = new Digitaalinenkello();
        Timer timer = new Timer();
        timer.addObserver(kello);
        
        Kellokone kellokone = new Kellokone(timer);
        kellokone.run();
        
    }
    
}
