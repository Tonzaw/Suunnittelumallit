/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Toni
 */
public class Kellokone implements Runnable {

    Timer timer;

    public Kellokone(Timer timer) {
        this.timer = timer;
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Kellokone.class.getName()).log(Level.SEVERE, null, ex);
            }
            timer.tick();
        }

    }

}
