/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author Toni
 */
public class Jasper {
    
    Kengät kengät = null;
    Lippalakki lippis = null;
    Paita paita = null;
    Housut housut = null;
    
    public Jasper(Vaatetehdas tehdas) {
        kengät = tehdas.luoKengät();
        lippis = tehdas.luoLippalakki();
        paita = tehdas.luoPaita();
        housut = tehdas.luoHousut();
    }

    public void kerroVaatteet(){
        System.out.println("Minulla on päällä: " + paita + ", " + housut + ", " + lippis + ", " + kengät);
    }
    
}