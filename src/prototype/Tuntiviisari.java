/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Toni
 */
public class Tuntiviisari extends ViisariIF implements  Cloneable{

    private int hours = 0;

    public void tick() {
        if (hours == 23) {
            hours = 0;
        } else {
            hours++;
        }
    }

    public int getTime() {
        return hours;
    }

    public void setTime(int time) {
        if (time >= 0 && time <= 23) {
            hours = time;
        } else {
            System.out.println("Hours need to be between 0 and 23");
        }
    }

}
