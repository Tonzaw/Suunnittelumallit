/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Toni
 */
public class Sekuntiviisari extends ViisariIF implements  Cloneable {
    
    private int seconds;

    public void tick() {
        if (seconds == 59){
            seconds = 0;
        } else {
            seconds++;
        }
    }

    public int getTime() {
        return seconds;
    }
    
    public void setTime(int time) {
        if (time >= 0 && time <= 59) {
            seconds = time;
        } else {
            System.out.println("Seconds need to be between 0 and 59");
        }
    }
}
