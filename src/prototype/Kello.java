/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Toni
 */
public class Kello implements Cloneable {

    Tuntiviisari tv = new Tuntiviisari();
    Minuuttiviisari mv = new Minuuttiviisari();
    Sekuntiviisari sv = new Sekuntiviisari();

    public Kello() {

    }

    @Override
    public Kello clone() {
        //matalakopio:
        Kello kello = null;
        try {
            kello = (Kello) super.clone();
            kello.tv = (Tuntiviisari) tv.clone();
            kello.mv = (Minuuttiviisari) mv.clone();
            kello.sv = (Sekuntiviisari) sv.clone();
        } catch (CloneNotSupportedException e) {
        }
        return kello;
    }
    
    public void setTime(int hours, int minutes, int seconds){
        tv.setTime(hours);
        mv.setTime(minutes);
        sv.setTime(seconds);
    }
    
    public void printTime(){
        System.out.println(tv.getTime()+"."+mv.getTime()+"."+sv.getTime());
    }

    void tick() {
        sv.tick();
        if (sv.getTime() == 0){
            mv.tick();
            if (mv.getTime() == 0){
                tv.tick();
            }
        }
    }

}
