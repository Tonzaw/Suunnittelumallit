/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Kello kello = new Kello();
        
        kello.setTime(12, 15, 0);
        System.out.println("Kellonaika:");
        kello.printTime();
        
        Kello klooni = kello.clone();
        System.out.println("Kloonin kellonaika:");
        klooni.printTime();
        
        System.out.println("Vaihdetaan kloonin kellonaika...");
        
        klooni.setTime(13, 30, 58);
        System.out.println("Kloonin kellonaika:");
        klooni.printTime();
        
        
        System.out.println("Laitetaan kellot päälle");
        
        kello.tick();
        klooni.tick();
        System.out.println("Kellonaika:");
        kello.printTime();
        System.out.println("Kloonin kellonaika:");
        klooni.printTime();
        
        kello.tick();
        klooni.tick();
        System.out.println("Kellonaika:");
        kello.printTime();
        System.out.println("Kloonin kellonaika:");
        klooni.printTime();
        
        kello.tick();
        klooni.tick();
        System.out.println("Kellonaika:");
        kello.printTime();
        System.out.println("Kloonin kellonaika:");
        klooni.printTime();
        
    }
    
}
