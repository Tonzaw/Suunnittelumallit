/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Toni
 */
public class Minuuttiviisari extends ViisariIF implements  Cloneable {
    
    private int minutes = 0;

    public void tick() {
        if (minutes == 59){
            minutes = 0;
        } else {
            minutes++;
        }
    }

    public int getTime() {
        return minutes;
    }
    
    public void setTime(int time) {
        if (time >= 0 && time <= 59) {
            minutes = time;
        } else {
            System.out.println("Minutes need to be between 0 and 59");
        }
    }
}
