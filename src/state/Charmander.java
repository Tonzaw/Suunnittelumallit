/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Toni
 */
public final class Charmander extends State {
    
    private static final Charmander INSTANCE = new Charmander();
    
    private Charmander() {
        
    }
    
    public static Charmander getInstance() {
        return INSTANCE;
    }
    
    @Override
    public void juokseKarkuun(){
        System.out.println("Charmander: Apuaaaa!");
    }
    
    @Override
    public void puolustaudu(){
        System.out.println("Charmander: Haha, ei osunu!");
    }
    
    @Override
    public void hyökkää(){
        System.out.println("Charmander: Ota tästä!");
        
    }
    
}
