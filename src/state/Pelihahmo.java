/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Toni
 */
public class Pelihahmo {
    private State state;
    
    public Pelihahmo(){
        state = Charmander.getInstance();
    }
    
    protected void vaihdaTilaa (State s) {
        state = s;
    }
    
    public void juokseKarkuun(){
        state.juokseKarkuun();
    }
    
    public void puolustaudu(){
        state.puolustaudu();
    }
    
    public void hyökkää(){
        state.hyökkää();
    }

    
}
