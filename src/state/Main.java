/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        final Pelihahmo player = new Pelihahmo();
        
        player.juokseKarkuun();
        player.puolustaudu();
        player.hyökkää();
        
        player.vaihdaTilaa(Charmeleon.getInstance());
        player.juokseKarkuun();
        player.puolustaudu();
        player.hyökkää();
        
        
        player.vaihdaTilaa(Charizard.getInstance());
        player.juokseKarkuun();
        player.puolustaudu();
        player.hyökkää();
        
    }
    
}
