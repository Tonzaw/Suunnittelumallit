/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Toni
 */
public final class Charizard extends State {
    
    private static final Charizard INSTANCE = new Charizard();
    
    private Charizard() {
        
    }
    
    public static Charizard getInstance() {
        return INSTANCE;
    }
    
    @Override
    public void juokseKarkuun(){
        System.out.println("Charizard: Apuaaaa!");
    }
    
    @Override
    public void puolustaudu(){
        System.out.println("Charizard: Haha, ei osunu!");
    }
    
    @Override
    public void hyökkää(){
        System.out.println("Charizard: Ota tästä!");
    }
    
}
