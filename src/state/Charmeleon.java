/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Toni
 */
public final class Charmeleon extends State {
    
    private static final Charmeleon INSTANCE = new Charmeleon();
    
    private Charmeleon() {
        
    }
    
    public static Charmeleon getInstance() {
        return INSTANCE;
    }
    
    @Override
    public void juokseKarkuun(){
        System.out.println("Charmeleon: Apuaaaa!");
    }
    
    @Override
    public void puolustaudu(){
        System.out.println("Charmeleon: Haha, ei osunu!");
    }
    
    @Override
    public void hyökkää(){
        System.out.println("Charmeleon: Ota tästä!");
    }
    
}
