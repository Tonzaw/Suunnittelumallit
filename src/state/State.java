/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author Toni
 */
abstract class State {
    void juokseKarkuun(){};
    void hyökkää(){};
    void puolustaudu(){};
    void vaihdaTilaa(Pelihahmo hahmo, State s){
        hahmo.vaihdaTilaa(s);
    };
}
