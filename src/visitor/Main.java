/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ConcreteVisitor visitor = new ConcreteVisitor();
        Pelihahmo p = new Pelihahmo(visitor);
        
        for (int i= 0; i < 3; i++){
            p.hyökkää();
        }
        p.puolustaudu();
        p.juokseKarkuun();
        
        for (int i= 0; i < 5; i++){
            p.hyökkää();
        }
        p.puolustaudu();
        p.juokseKarkuun();
        
        p.hyökkää();
        p.puolustaudu();
        p.juokseKarkuun();
    }
    
}
