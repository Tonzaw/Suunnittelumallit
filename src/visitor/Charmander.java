/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Toni
 */
public final class Charmander extends State {
    
    private static final Charmander INSTANCE = new Charmander();
    
    private Charmander() {
        
    }
    
    public static Charmander getInstance() {
        return INSTANCE;
    }
    
    @Override
    public void juokseKarkuun(Pelihahmo pokemon, ConcreteVisitor visitor){
        System.out.println("Charmander: Apuaaaa!");
        visitor.visit(pokemon, this);
    }
    
    @Override
    public void puolustaudu(Pelihahmo pokemon, ConcreteVisitor visitor){
        System.out.println("Charmander: Haha, ei osunu!");
        visitor.visit(pokemon, this);
    }
    
    @Override
    public void hyökkää(Pelihahmo pokemon, ConcreteVisitor visitor){
        System.out.println("Charmander: Ota tästä!");
        visitor.visit(pokemon, this);
    }
    
}
