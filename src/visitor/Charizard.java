/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Toni
 */
public final class Charizard extends State {

    private static final Charizard INSTANCE = new Charizard();

    private Charizard() {

    }


    public static Charizard getInstance() {
        return INSTANCE;
    }

    @Override
    public void juokseKarkuun(Pelihahmo pokemon, ConcreteVisitor visitor) {
        System.out.println("Charizard: Apuaaaa!");
        visitor.visit(pokemon, this);
    }

    @Override
    public void puolustaudu(Pelihahmo pokemon, ConcreteVisitor visitor) {
        System.out.println("Charizard: Haha, ei osunu!");
        visitor.visit(pokemon, this);
    }

    @Override
    public void hyökkää(Pelihahmo pokemon, ConcreteVisitor visitor) {
        System.out.println("Charizard: Ota tästä!");
        visitor.visit(pokemon, this);
    }

}
