/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Toni
 */
public interface PokemonVisitor {
    void visit(Pelihahmo pokemon, Charizard charizard);
    void visit(Pelihahmo pokemon, Charmeleon charmeleon);
    void visit(Pelihahmo pokemon, Charmander charmander);

}
