/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Toni
 */
public class Pelihahmo {
    
    private State state;
    private final ConcreteVisitor visitor;
    private int attackCounter;
    
    public Pelihahmo(ConcreteVisitor visitor){
        state = Charmander.getInstance();
        this.visitor = visitor;
        attackCounter = 0;
    }
    
    protected void vaihdaTilaa (State s) {
        state = s;
        attackCounter = 0;
    }
    
    public void juokseKarkuun(){
        state.juokseKarkuun(this, visitor);
    }
    
    public void puolustaudu(){
        state.puolustaudu(this, visitor);
    }
    
    public void hyökkää(){
        state.hyökkää(this, visitor);
        attackCounter++;
    }
    
    public int getAttackCounter(){
        return attackCounter;
    }

    
}
