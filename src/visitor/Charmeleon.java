/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Toni
 */
public final class Charmeleon extends State {
    
    private static final Charmeleon INSTANCE = new Charmeleon();
    
    private Charmeleon() {
        
    }
    
    public static Charmeleon getInstance() {
        return INSTANCE;
    }
    
    @Override
    public void juokseKarkuun(Pelihahmo pokemon, ConcreteVisitor visitor){
        System.out.println("Charmeleon: Apuaaaa!");
        visitor.visit(pokemon, this);
    }
    
    @Override
    public void puolustaudu(Pelihahmo pokemon, ConcreteVisitor visitor){
        System.out.println("Charmeleon: Haha, ei osunu!");
        visitor.visit(pokemon, this);
    }
    
    @Override
    public void hyökkää(Pelihahmo pokemon, ConcreteVisitor visitor){
        System.out.println("Charmeleon: Ota tästä!");
        visitor.visit(pokemon, this);
    }
    
}
