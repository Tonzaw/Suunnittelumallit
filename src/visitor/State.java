/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Toni
 */
abstract class State {
    void juokseKarkuun(Pelihahmo pokemon, ConcreteVisitor visitor){};
    void hyökkää(Pelihahmo pokemon, ConcreteVisitor visitor){};
    void puolustaudu(Pelihahmo pokemon, ConcreteVisitor visitor){};
    void vaihdaTilaa(Pelihahmo pokemon, State s){
        pokemon.vaihdaTilaa(s);
    };
}
