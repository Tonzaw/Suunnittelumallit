/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author Toni
 */
public class ConcreteVisitor implements PokemonVisitor {

    public void visit(Pelihahmo pokemon, Charizard charizard) {

    }

    public void visit(Pelihahmo pokemon, Charmeleon charmeleon) {
        if (pokemon.getAttackCounter() >= 5) {
            charmeleon.vaihdaTilaa(pokemon, Charizard.getInstance());
            System.out.println("Your pokemon evolved!");
        }
    }

    public void visit(Pelihahmo pokemon, Charmander charmander) {
        if (pokemon.getAttackCounter() >= 3) {
            charmander.vaihdaTilaa(pokemon, Charmeleon.getInstance());
            System.out.println("Your pokemon evolved!");
        }
    }

}
