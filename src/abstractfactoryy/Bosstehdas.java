/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactoryy;

/**
 *
 * @author Toni
 */
public class Bosstehdas implements Vaatetehdas {

    public Kengät luoKengät() {
        return new Bosskengät();
    }

    public Housut luoHousut() {
        return new Bosshousut();
    }

    public Paita luoPaita() {
        return new Bosspaita();
    }

    public Lippalakki luoLippalakki() {
        return new Bosslakki();
    }
    
}
