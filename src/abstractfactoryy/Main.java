package abstractfactoryy;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Class c = null;
        Vaatetehdas tehdas = null;
        Properties properties = new Properties();
        
        try {
            properties.load(new FileInputStream("tehdas.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            //luetaan toteuttava tehdas properties-tiedostosta
            c = Class.forName(properties.getProperty("tehdas"));
            tehdas = (Vaatetehdas) c.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Jasper jasper = new Jasper(tehdas);
        jasper.kerroVaatteet();

    }

}
