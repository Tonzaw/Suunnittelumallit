/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactoryy;

/**
 *
 * @author Toni
 */
public class Adidastehdas implements Vaatetehdas {

    public Kengät luoKengät() {
        return new Adidaskengät();
    }

    public Housut luoHousut() {
        return new Adidashousut();
    }

    public Paita luoPaita() {
        return new Adidaspaita();
    }

    public Lippalakki luoLippalakki() {
        return new Adidaslakki();
    }
    
}
