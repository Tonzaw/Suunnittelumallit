/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Toni
 */
public class PackingMaterialGoods implements Goods {

    String nimi;

    public PackingMaterialGoods(String nimi) {
        this.nimi = nimi;
    }

    public String getName() {
        return nimi;
    }

}
