/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Toni
 */
public class RawMaterialStore implements Store {

    public Goods getGoods() {
        return new RawMaterialGoods("Raw material goods");
    }
    
}
