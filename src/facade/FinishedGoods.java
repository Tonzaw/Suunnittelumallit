/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Toni
 */
public class FinishedGoods implements Goods {

    String nimi;

    public FinishedGoods(String nimi) {
        this.nimi = nimi;
    }

    public String getName() {
        return nimi;
    }

}
