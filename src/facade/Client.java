/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Toni
 */
public class Client {

    /**
     * to get raw materials
     * @param args
     */
    public static void main(String[] args) {
        StoreKeeper keeper = new StoreKeeper();
        
        Goods goods = keeper.getGoods("RawMaterials");
        Goods goods2 = keeper.getGoods("Packaging");
        
        System.out.println(goods.getName());
        System.out.println(goods2.getName());
    }
}
