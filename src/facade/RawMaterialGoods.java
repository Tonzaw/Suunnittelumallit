/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Toni
 */
public class RawMaterialGoods implements Goods {

    
    String nimi;

    public RawMaterialGoods(String nimi) {
        this.nimi = nimi;
    }
    
    public String getName() {
        return nimi;
    }
    
}
