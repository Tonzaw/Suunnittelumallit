/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AudioPlayer audioPlayer = new AudioPlayer();

        audioPlayer.play("mp3", "hello.mp3");
        audioPlayer.play("mp4", "alone.mp4");
        audioPlayer.play("vlc", "jes.vlc");
        audioPlayer.play("avi", "go away.avi");
    }

}
