/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        List list = new ArrayList();
        list.add("Toni");
        list.add("Toni");
        list.add("Toni");
        list.add("Toni");
        list.add("Toni");
        list.add("Toni");
        list.add("Toni");
        list.add("Toni");
        list.add("Toni");
        list.add("Toni");
        
        /*
        String[] list2 = new String[9];
        for (int i = 0; i <= list2.length; i++){
            list2[i] = "Toni";
        }
        */
        
        
        System.out.println("Strategy one:");
        
        StrategyOne s1 = new StrategyOne();
        String listString = s1.listToString(list);
        System.out.println(listString);
        
        System.out.println("Strategy two:");
        
        StrategyTwo s2 = new StrategyTwo();
        String listString2 = s2.listToString(list);
        System.out.println(listString2);
        
        System.out.println("Strategy three:");
        
        StrategyThree s3 = new StrategyThree();
        String listString3 = s3.listToString(list);
        System.out.println(listString3);
        
    }
    
}
