/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.List;

/**
 *
 * @author Toni
 */
public class StrategyThree implements ListConverter {
    
    @Override
    public String listToString(List list) {
        String stringList = null;

        for (int i = 0; i < list.size(); i++) {
            String element = (String) list.get(i);
            if (stringList == null){
                stringList = element;
            } else {
                if (i % 3 == 0){
                    stringList = stringList + "\n" + element;
                } else {
                    stringList = stringList + " " + element;
                }
            }
        }

        return stringList;
    }
    
}
