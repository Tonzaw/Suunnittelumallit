/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Toni
 */
public class StrategyOne implements ListConverter {

    @Override
    public String listToString(List list) {
        String stringList = null;

        Iterator itr = list.iterator();

        while (itr.hasNext()) {
            String element = (String) itr.next();
            if (stringList == null) {
                stringList = element;
            } else {
                stringList = stringList + "\n" + element;
            }
        }

        return stringList;
    }

}
