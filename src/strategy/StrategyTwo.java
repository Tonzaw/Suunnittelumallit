/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.List;

/**
 *
 * @author Toni
 */
public class StrategyTwo implements ListConverter {
    
    @Override
    public String listToString(List list) {
        String stringList = null;
        String[] list2 = new String[list.size()];
        
        for (int i = 0; i < list.size(); i++){
            list2[i] = (String) list.get(i);
            
            String element = (String) list2[i];
            if (stringList == null){
                stringList = element;
            } else {
                if (i % 2 == 0){
                    stringList = stringList + "\n" + element;
                } else {
                    stringList = stringList + " " + element;
                }
            }
        }

        return stringList;
    }
    
}