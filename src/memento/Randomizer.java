/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import java.util.Random;

/**
 *
 * @author Toni
 */
public class Randomizer {
    
    private Random random;
    
    public Randomizer(){
        random = new Random();
    }
    
    public void liityPeliin(Pelaaja pelaaja){
        pelaaja.setMemento(new Memento(random.nextInt(10)+1));
    }
    
    public boolean kasitteleArvaus(int arvaus, Memento memento){
        return memento.vertaaArvausta(arvaus);
    }
    
}
