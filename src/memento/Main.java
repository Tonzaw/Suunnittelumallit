/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Randomizer rand = new Randomizer();
        
        Pelaaja p1 = new Pelaaja(rand, "Toni");
        Pelaaja p2 = new Pelaaja(rand, "Jaska");
        Pelaaja p3 = new Pelaaja(rand, "Jorma Uotinen");
        Pelaaja p4 = new Pelaaja(rand, "Kummola");
        Pelaaja p5 = new Pelaaja(rand, "Timo Jutila");
        
        p1.start();
        p2.start();
        p3.start();
        p4.start();
        p5.start();
        
    }
    
}
