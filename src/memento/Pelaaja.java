/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

/**
 *
 * @author Toni
 */
public class Pelaaja extends Thread {
    
    private Memento memento;
    private final Randomizer rand;
    private final String nimi;
    
    public Pelaaja(Randomizer rand, String nimi){
        this.rand = rand;
        this.nimi = nimi;
    }
    
    @Override
    public void run(){
        rand.liityPeliin(this);
        int arvaus = 1;
        while (true){
            if (rand.kasitteleArvaus(arvaus, memento)){
                System.out.println(nimi + " arvasi oikein!");
                break;
            }
            arvaus++;
        }
    }
    
    public void setMemento(Memento memento) {
        this.memento = memento;
    }
    
}
