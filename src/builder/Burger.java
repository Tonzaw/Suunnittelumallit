/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author Toni
 */
class Burger {
    
    private Buns buns;
    private Steak steak;
    private Lettuce lettuce;

    public void setBuns(Buns buns) {
        this.buns = buns;
    }

    public void setSteak(Steak steak) {
        this.steak = steak;
    }

    public void setLettuce(Lettuce lettuce) {
        this.lettuce = lettuce;
    }

    public Buns getBuns() {
        return buns;
    }

    public Steak getSteak() {
        return steak;
    }

    public Lettuce getLettuce() {
        return lettuce;
    }
    
    
    
}
