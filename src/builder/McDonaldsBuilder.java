/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author Toni
 */
public class McDonaldsBuilder extends BurgerBuilder {

    @Override
    public void buildBuns() {
        burgerlist.add("buns");
    }

    @Override
    public void buildSteak() {
        burgerlist.add("steak");
    }

    @Override
    public void buildLettuce() {
        burgerlist.add("lettuce");
    }
    
    
    
}
