/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Director director = new Director();
        BurgerBuilder mcDonalds = new McDonaldsBuilder();
        BurgerBuilder hesburger = new HesburgerBuilder();
        
        director.setBurgerBuilder(mcDonalds);
        director.constructBurger();
        
        List burger = director.getBurger();
        
        System.out.println("Your burger has: "+burger.get(0)+", "+ burger.get(1) +", "+ burger.get(2));
        
        
    }
    
}
