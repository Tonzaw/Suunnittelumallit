/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author Toni
 */
public class HesburgerBuilder extends BurgerBuilder {

    @Override
    public void buildBuns() {
        burgerlist.add(new Buns());
    }

    @Override
    public void buildSteak() {
        burgerlist.add(new Steak());
    }

    @Override
    public void buildLettuce() {
        burgerlist.add(new Lettuce());
    }
    
}
