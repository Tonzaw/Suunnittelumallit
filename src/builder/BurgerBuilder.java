/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Toni
 */
abstract class BurgerBuilder {
    
    protected List burgerlist;
    
    public List getBurger(){
        return burgerlist;
    }
    
    public void createNewBurgerProduct(){
        burgerlist = new ArrayList();
    }
    
    public abstract void buildBuns();
    public abstract void buildSteak();
    public abstract void buildLettuce();
    
}
