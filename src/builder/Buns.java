/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author Toni
 */
public class Buns implements BurgerPart {
    String name;

    public Buns() {
        name = "buns";
    }
    
    @Override
    public String getName() {
        return name;
    }
}
