/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author Toni
 */
public class Lettuce implements BurgerPart  {
    
    String name;

    public Lettuce() {
        name = "lettuce";
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    
}
