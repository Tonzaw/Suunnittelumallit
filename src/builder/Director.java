/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.List;

/**
 *
 * @author Toni
 */
class Director {
    private BurgerBuilder burgerBuilder;
    
    public void setBurgerBuilder(BurgerBuilder bb){
        burgerBuilder = bb;
    }
    
    public List getBurger(){
        return burgerBuilder.getBurger();
    }
    
    public void constructBurger(){
        burgerBuilder.createNewBurgerProduct();
        burgerBuilder.buildBuns();
        burgerBuilder.buildSteak();
        burgerBuilder.buildLettuce();
        
    }
}
