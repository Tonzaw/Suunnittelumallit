/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import java.util.ArrayList;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ArrayList<Image> valokuvakansio = new ArrayList();
        valokuvakansio.add((Image) new ProxyImage("HiRes_10MB_Photo1"));
        valokuvakansio.add((Image) new ProxyImage("HiRes_10MB_Photo2"));
        valokuvakansio.add((Image) new ProxyImage("HiRes_10MB_Photo3"));
        
        System.out.println("Valokuvakansion sisältö:");
        
        for (Image valokuvakansio1 : valokuvakansio) {
            valokuvakansio1.showData();
        }
        
        System.out.println("Näytetään kuvat:");
        
        for (Image valokuvakansio1 : valokuvakansio) {
            valokuvakansio1.displayImage();
        }
        
    }
    
}
