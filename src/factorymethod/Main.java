package factorymethod;

public class Main {

    public static void main(String[] args) {
        AterioivaOtus opettaja = new Opettaja();
        AterioivaOtus sami = new Sami();
        AterioivaOtus toni = new Toni();
        opettaja.aterioi();
        sami.aterioi();
        toni.aterioi();
    }
}
