/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author Toni
 */
public class Manager implements RaiseHandler {
    
    RaiseHandler nextHandler;
    
    public Manager () {
        nextHandler = new Boss();
    }

    public void handleRaise(double raise) {
        if(raise <= 0.02){
            System.out.println("Palkankorotuksesi hyväksyi Manager.");
        } else {
            nextHandler.handleRaise(raise);
        }
    }
    
}
