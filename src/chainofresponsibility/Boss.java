/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author Toni
 */
public class Boss implements RaiseHandler {

    RaiseHandler nextHandler;

    public Boss() {
        nextHandler = new CEO();
    }

    public void handleRaise(double raise) {
        if (raise > 0.02 && raise <= 0.05) {
            System.out.println("Palkankorotuksesi hyväksyi Boss.");
        } else {
            nextHandler.handleRaise(raise);
        }
    }

}
