/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        RaiseHandler manager = new Manager();
        
        System.out.println("Pyydetään 2% palkankorotusta:");
        double raise = 0.02;
        manager.handleRaise(raise);
        
        System.out.println("Pyydetään 3% palkankorotusta:");
        double raise2 = 0.03;
        manager.handleRaise(raise2);
        
        System.out.println("Pyydetään 6% palkankorotusta:");
        double raise3 = 0.06;
        manager.handleRaise(raise3);
    }
    
}
