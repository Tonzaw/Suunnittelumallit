/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

/**
 *
 * @author Toni
 */
public class FlipDownCommand implements Command {

    private WhiteScreen ws;

    public FlipDownCommand(WhiteScreen ws) {
        this.ws = ws;
    }

    @Override // Command
    public void execute() {
        ws.down();
    }
}
