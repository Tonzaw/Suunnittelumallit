/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

/**
 *
 * @author Toni
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        WhiteScreen screen = new WhiteScreen();
        Command switchUp = new FlipUpCommand(screen);
        Command switchDown = new FlipDownCommand(screen);
        WallButton button1 = new WallButton(switchUp);
        WallButton button2 = new WallButton(switchDown);
        button1.push();
        button2.push();

    }

}
